import * as july from './2022/2022_JULY.json' assert { type: 'json' };
import * as august from './2022/2022_AUGUST.json' assert { type: 'json' };
import * as september from './2022/2022_SEPTEMBER.json' assert { type: 'json' };
import * as october from './2022/2022_OCTOBER.json' assert { type: 'json' };

const getExtract = () => {
	const vsList = [
		...july.default.timelineObjects,
		...august.default.timelineObjects,
		...september.default.timelineObjects,
		...october.default.timelineObjects,
	]
		.filter((object) => object.placeVisit)
		.map((object) => object.placeVisit);
	console.log(
		JSON.stringify(getLocationExtract(['Vaishanvi Signature', 'Vaishnavi Signature'], vsList))
	);
};

const getLocationExtract = (locationNames, timelineObjects) => {
	const locationObjectsList = timelineObjects
		.filter((object) => {
			return locationNames.includes(object.location.name);
		})
		.map(
			(object) => ({
				...object.location,
				...object.duration,
				date: new Date(object.duration.startTimestamp).toLocaleDateString(
					'en-GB'
				),
				timespan:
					Math.round(
						((new Date(object.duration.endTimestamp) -
							new Date(object.duration.startTimestamp)) /
							(3600 * 1000) +
							Number.EPSILON) *
							100
					) / 100,
			}),
			2
		);

	return combineSameDayData(locationObjectsList);
};

const combineSameDayData = (locationObjectsList) => {
	const combinedlocationObjectList = locationObjectsList.reduce(
		(accumulator, currentValue) => {
			let groupKey = currentValue['date'];
			if (!accumulator[groupKey]) {
				accumulator[groupKey] = [];
			}
			accumulator[groupKey].push(currentValue);
			return accumulator;
		},
		{}
	);
	const combinedData = Object.keys(combinedlocationObjectList).map((key) => ({
		location: combinedlocationObjectList[key][0].name,
		date: key,
		hours: combinedlocationObjectList[key].reduce(
			(accumulator, currentValue) => {
				return accumulator + currentValue.timespan;
			},
			0
		),
	}));
	return combinedData
		.map((o) => ({ ...o, location: 'Vaishnavi Signature' }))
		.filter((o) => o.hours >= 2);
};

getExtract();
